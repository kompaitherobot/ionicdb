<?php


  use PHPMailer\PHPMailer\PHPMailer;
  use PHPMailer\PHPMailer\Exception;
  use PHPMailer\PHPMailer\SMTP;

  require __DIR__ . '/PHPMailer/Exception.php';
  require __DIR__ . '/PHPMailer/PHPMailer.php';
  require __DIR__ . '/PHPMailer/SMTP.php';


  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $postcontent = file_get_contents("php://input");
    $postData = json_decode($postcontent, true);

    if ($postData) {
        $user_username = $postData['username'];
        $user_email = $postData['email'];
        $user_bcc = $postData['bcc'];
        $user_message = $postData['message'];
        $user_subject = $postData['subject'];
        $user_password = $postData['password'];
        $user_robotname = $postData['robotname'];
        $user_url = $postData['url'];

        $mail = new PHPMailer(true);

        try {
            $mail->isSMTP();
            $mail->Host = 'smtp.gmail.com';
            $mail->SMTPAuth = true;
            $mail->Username = $user_username;
            $mail->Password = $user_password;
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
            $mail->Port = 465;

            $mail->setFrom($user_username, $user_robotname);
            $mail->addAddress($user_email);

            if (is_array($user_bcc)) {
                foreach ($user_bcc as $bcc) {
                    if (!empty($bcc)) {
                        $mail->addBCC($bcc);
                    }
                }
            }

            $mail->CharSet = 'UTF-8';

            if (!empty($user_url)) {
                $base = explode('data:image/png;base64,', $user_url);
                if (isset($base[1])) {
                    $mail->AddStringAttachment(base64_decode($base[1]), "attachment.png", "base64", "image/png");
                }
            }

            $mail->isHTML(true);
            $mail->Subject = $user_subject;
            $mail->Body = $user_message;
            $mail->AltBody = strip_tags($user_message);

            $mail->send();
            echo json_encode("OK");
        } catch (Exception $e) {
            error_log('Mailer Error: ' . $e->getMessage());
            echo json_encode("Message could not be sent. Mailer Error: {$e->getMessage()}");
        }
    } else {
        echo json_encode("Invalid JSON");
    }
} else {
    echo json_encode("Invalid request method");
}

?>