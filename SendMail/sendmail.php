<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\SMTP;

require __DIR__ . '/PHPMailer/Exception.php';
require __DIR__ . '/PHPMailer/PHPMailer.php';
require __DIR__ . '/PHPMailer/SMTP.php';

$message = array();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['username'])) {
        // Handling form-urlencoded POST request
        $user_username = $_POST['username'];
        $user_email = $_POST['email'];
        $user_message = $_POST['message'];
        $user_bcc = $_POST['bcc'];
        $user_subject = $_POST['subject'];
        $user_password = $_POST['password'];
        $user_robotname = $_POST['robotname'];
    } else {
        // Handling JSON POST request
        $postcontent = file_get_contents("php://input");
        $postData = json_decode($postcontent);

        if ($postData) {
            $user_username = $postData->username;
            $user_email = $postData->email;
            $user_bcc = $postData->bcc;
            $user_message = $postData->message;
            $user_subject = $postData->subject;
            $user_password = $postData->password;
            $user_robotname = $postData->robotname;
        } else {
            echo json_encode("Invalid JSON");
            error_log('Invalid JSON received');
            exit;
        }
    }

    // Create an instance; passing `true` enables exceptions
    $mail = new PHPMailer(true);

    try {
        // Server settings
        $mail->SMTPDebug = SMTP::DEBUG_SERVER;      // Enable verbose debug output
        $mail->isSMTP();                           // Send using SMTP
        $mail->Host       = 'smtp.gmail.com';      // Set the SMTP server to send through
        $mail->SMTPAuth   = true;                  // Enable SMTP authentication
        $mail->Username   = $user_username;        // SMTP username
        $mail->Password   = $user_password;        // SMTP password
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS; // Enable implicit TLS encryption
        $mail->Port       = 465;                   // TCP port to connect to

        // Recipients
        $mail->setFrom($user_username, $user_robotname);
        $mail->addAddress($user_email);

        if (is_array($user_bcc)) {
            foreach ($user_bcc as $value) {
                if (!empty($value)) {
                    $mail->addBCC($value);
                }
            }
        } else {
            if (!empty($user_bcc)) {
                $mail->addBCC($user_bcc);
            }
        }

        $mail->CharSet = 'UTF-8';
        $mail->Encoding = 'base64';

        // Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $user_subject;
        $mail->Body    = $user_message;
        $mail->AltBody = strip_tags($user_message);

        $mail->send();
        echo json_encode("OK");
        error_log('Message has been sent');
    } catch (phpmailerException $e) {
        error_log('PHPMailer Exception: ' . $e->errorMessage());
        echo json_encode($e->errorMessage());
    } catch (Exception $e) {
        error_log('General Exception: ' . $e->getMessage());
        echo json_encode($e->getMessage());
    }
} else {
    echo json_encode("Invalid request method");
    error_log('Invalid request method');
}
?>
