<?php
// Set the CORS headers to allow all origins
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
header("Access-Control-Allow-Headers: *");

// Make the request to Google
$response = file_get_contents('https://www.google.com');

// Forward Google's response to the client
echo $response;
?>
