<?php
function isSiteReachable($url) {
    $context = stream_context_create(array(
        'http' => array(
            'method' => 'GET',
            'header' => 'Cache-Control: no-cache' . "\r\n" .
                        'Pragma: no-cache' . "\r\n",
            'timeout' => 5 // Temps d'attente de 5 secondes
        )
    ));

    // Tentative de récupérer le contenu du site en utilisant le contexte spécifié
    $contents = @file_get_contents($url, false, $context);

    // Vérifie si le contenu a été récupéré avec succès
    return $contents !== false;
}

// Test d'accès à un site
$siteURL = 'https://example.com'; // Remplacez "example.com" par l'URL du site que vous souhaitez tester
if (isSiteReachable($siteURL)) {
    echo "true";
} else {
    echo "false";
}
?>